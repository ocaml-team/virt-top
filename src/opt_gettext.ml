(* This file is generated automatically by ./configure. *)
module Gettext = Gettext.Program (
  struct
    let textdomain = "virt-top"
    let codeset = None
    let dir = None
    let dependencies = []
  end
) (GettextStub.Native)
